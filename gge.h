///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.h
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#pragma once 
const double gasoline_in_a_joule       = 1/(1.213e8);
const char GASOLINE      = 'g';

extern double fromGasolineGallonEquivalentToJoule( double gasoline );
extern double fromJouleToGasolineGallonEquivalent( double joule );

