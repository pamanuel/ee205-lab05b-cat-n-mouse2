///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.cpp
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

#include "foe.h"

double fromFoeToJoule( double foe ) {
   return foe / foe_in_a_joule ;  ///
}
double fromJouleToFoe( double joule ) {
   return joule * foe_in_a_joule;  //
}

