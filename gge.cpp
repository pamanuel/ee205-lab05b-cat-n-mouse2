///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 05_Feb_2022
//////////////////////////////////////////////////////////////////////////////

#include "gge.h"

double fromGasolineGallonEquivalentToJoule( double gasoline ) {
   return gasoline / gasoline_in_a_joule; ///
}
double fromJouleToGasolineGallonEquivalent( double joule ) {
   return joule * gasoline_in_a_joule;  ///
}

