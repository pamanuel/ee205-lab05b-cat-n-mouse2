///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file foe.h
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//
#pragma once 
const double foe_in_a_joule            = 1/(1e24);
const char FOE           = 'f';

extern double fromFoeToJoule( double foe ) ;
extern double fromJouleToFoe( double joule ) ;

