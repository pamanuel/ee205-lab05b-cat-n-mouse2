###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Patrick Manuel <pamanuel@hawaii.edu>
### @date 5_Feb_2022
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower
all: $(TARGET)

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp
megaton.o: megaton.cpp megaton.h
	$(CC) $(CFLAGS) -c megaton.cpp
gge.o: gge.cpp gge.h
	$(CC) $(CFLAGS) -c gge.cpp
foe.o: foe.cpp foe.h
	$(CC) $(CFLAGS) -c foe.cpp
cat.o: cat.cpp cat.h
	$(CC) $(CFLAGS) -c cat.cpp

catPower.o: catPower.cpp ev.h megaton.h gge.h foe.h cat.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o ev.o megaton.o gge.o foe.o cat.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o megaton.o gge.o foe.o cat.o

clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-24 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x | grep -q "Unknown toUnit \[x\]"
	@echo "All tests pass"

