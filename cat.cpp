///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.cpp
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "cat.h"
double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}
double fromJouleToCatPower( double joule){
   return 0.0;
}

