///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file megaton.cpp
/// @version 1.0
///
/// @author Patrick Manuel <pamanuel@hawaii.edu>
/// @date 05_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "megaton.h"

double fromMegatonToJoule( double megaton ) {
   return megaton / megaton_in_a_joule ;
}
double fromJouleToMegaton( double joule ) {
   return joule * megaton_in_a_joule;  ///
}

